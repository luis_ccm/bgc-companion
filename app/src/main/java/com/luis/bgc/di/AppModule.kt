package com.luis.bgc.di

import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.luis.bgc.data.room.AppDatabase
import com.luis.bgc.data.room.dao.GameDao
import com.luis.bgc.data.room.dao.TrendingGamesBGGDao
import com.luis.bgc.data.room.dao.UserCollectionDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import javax.inject.Singleton

const val BGG_BASE_URL = "https://boardgamegeek.com/xmlapi2/"
const val BGC_SHARED_PREFERENCE = "bgc_shared_preference"

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    @Singleton
    fun providesRetrofit(client: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(BGG_BASE_URL)
        .client(client)
        .addConverterFactory(SimpleXmlConverterFactory.createNonStrict())
        .build()

    @Provides
    @Singleton
    fun providesRetrofitClient(): OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().apply { setLevel(HttpLoggingInterceptor.Level.BODY) })
        .build()

    @Singleton
    @Provides
    fun provideSharedPreference(@ApplicationContext context: Context): SharedPreferences {
        return context.getSharedPreferences(BGC_SHARED_PREFERENCE, Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            "BoardGameCompanion"
        ).build()
    }

    @Provides
    fun provideGameDao(appDatabase: AppDatabase): GameDao {
        return appDatabase.gameDao()
    }

    @Provides
    fun provideUserCollectionDao(appDatabase: AppDatabase): UserCollectionDao {
        return appDatabase.userCollectionDao()
    }

    @Provides
    fun provideTrendingGamesBGGDao(appDatabase: AppDatabase): TrendingGamesBGGDao {
        return appDatabase.trendingGameBGGDao()
    }
}
