package com.luis.bgc.ui.screens.game

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavHostController
import com.luis.bgc.domain.use_case.GetGameById
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GameViewModel @Inject constructor(
    private val getGameById: GetGameById
) : ViewModel() {

    private val _uiStateFlow: MutableStateFlow<GameUiState> = MutableStateFlow(GameUiState.Loading)
    val uiStateFlow: StateFlow<GameUiState>
        get() = _uiStateFlow

    fun loadGame(gameId: String) {
        viewModelScope.launch {
            getGameById(gameId)
                .catch { Log.d("luis TEST", "FAILURE $it") }
                .collect { _uiStateFlow.emit(GameUiState.Success(it)) }
        }
    }

    fun onBackPressed(appNavController: NavHostController) {
        appNavController.navigateUp()
    }
}
