package com.luis.bgc.ui.component

import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AlertWithInput(
    title: String = "",
    hint: String = "",
    initialText: String = "",
    onDismiss: () -> Unit = {},
    onSubmit: (String) -> Unit = {}
) {
    val text = remember { mutableStateOf(initialText) }

    AlertDialog(
        title = { if (title.isNotBlank()) Text(text = title) },
        onDismissRequest = { onDismiss() },
        text = {
            TextField(
                value = text.value,
                onValueChange = { text.value = it },
                label = { Text(text = hint) },
                modifier = Modifier
                    .fillMaxWidth()
            )
        },
        confirmButton = {
            TextButton(onClick = { onSubmit(text.value) }) { Text(text = "Submit") }
        }
    )
}
