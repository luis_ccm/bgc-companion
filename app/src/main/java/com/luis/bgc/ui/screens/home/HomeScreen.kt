package com.luis.bgc.ui.screens.home

import androidx.compose.animation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material.icons.outlined.ThumbUp
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavHostController
import com.luis.bgc.R
import com.luis.bgc.domain.model.Game
import com.luis.bgc.ui.component.BoxWithPattern
import com.luis.bgc.ui.component.GamesGrid
import com.luis.bgc.ui.util.AppThemeWithSurface
import com.luis.bgc.ui.util.ProgressScreen

@Composable
fun HomeScreen(
    appNavController: NavHostController,
    modifier: Modifier = Modifier,
    viewModel: HomeViewModel = hiltViewModel(),
) {
    val uiState by viewModel.uiStateFlow.collectAsStateWithLifecycle()

    BoxWithPattern() {
        HomeScreen(
            uiState = uiState,
            modifier = modifier,
            onItemClick = {
                viewModel.onItemClick(appNavController, it)
            }
        )
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun HomeScreen(
    uiState: HomeUiState,
    modifier: Modifier = Modifier,
    onItemClick: (Game) -> Unit = {},
) {
    AnimatedContent(
        targetState = uiState,
        transitionSpec = { fadeIn() with fadeOut() }
    ) { targetState ->
        when (targetState) {
            HomeUiState.Loading -> ProgressScreen()
            is HomeUiState.Success -> HomeScreenContent(
                games = targetState.games,
                modifier = modifier,
                onItemClick = onItemClick
            )
        }
    }
}

@Composable
fun HomeScreenContent(
    games: List<Game>,
    modifier: Modifier = Modifier,
    onItemClick: (Game) -> Unit = {},
) {
    Column(modifier = modifier) {
        Text(
            modifier = Modifier.padding(16.dp),
            text = stringResource(R.string.home_highlight_title),
            style = MaterialTheme.typography.titleLarge
        )

        if (games.isNotEmpty()) {
            GamesGrid(
                games = games,
                onItemClick = onItemClick,
                ranked = true,
                gridItemMinSize = 80.dp,
                gridItemHeight = 100.dp
            )
        } else {
            Row(
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxSize(),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Icon(
                    imageVector = Icons.Outlined.ThumbUp,
                    contentDescription = null
                )
                Text(
                    modifier = Modifier
                        .padding(start = 16.dp)
                        .widthIn(max = 200.dp),
                    text = stringResource(id = R.string.collection_empty_list),
                    style = MaterialTheme.typography.labelSmall
                )
            }
        }
    }
}

@Preview
@Composable
fun HomeScreenPreview() {
    AppThemeWithSurface {
        HomeScreen(uiState = HomeUiState.Success(provideGameList()))
    }
}

fun provideGameList(): List<Game> = listOf(
    Game(
        image = "https://cf.geekdo-images.com/yetm7Pm1q8pHZHKzTdbRzQ__imagepage/img/8EJjHxMK5fnSdDBB0eZimxeiYBA=/fit-in/900x600/filters:no_upscale():strip_icc()/pic6555174.png",
        name = "Game A with a very large board game name",
        yearPublished = "2022",
        description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultrices sapien pharetra quam rutrum, et aliquam libero facilisis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam risus dui, semper sit amet lectus ac, euismod vestibulum ligula. Nulla quis quam nec lectus condimentum tristique in sit amet nibh. Vestibulum dui orci, facilisis non mauris eu, maximus venenatis quam. Suspendisse ut diam cursus, consequat diam non, pellentesque quam. Praesent vitae risus tincidunt, hendrerit turpis varius, porta augue. Cras vehicula hendrerit molestie.",
        maxPlayers = "4",
        playingTime = "120",
        minAge = "14",
        designers = listOf("Person 1"),
        artists = listOf("Person 1"),
        publishers = listOf("Person 1"),
    ),
    Game(
        image = "https://cf.geekdo-images.com/yetm7Pm1q8pHZHKzTdbRzQ__imagepage/img/8EJjHxMK5fnSdDBB0eZimxeiYBA=/fit-in/900x600/filters:no_upscale():strip_icc()/pic6555174.png",
        name = "Game A with a very large board game name",
        yearPublished = "2022",
        description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultrices sapien pharetra quam rutrum, et aliquam libero facilisis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam risus dui, semper sit amet lectus ac, euismod vestibulum ligula. Nulla quis quam nec lectus condimentum tristique in sit amet nibh. Vestibulum dui orci, facilisis non mauris eu, maximus venenatis quam. Suspendisse ut diam cursus, consequat diam non, pellentesque quam. Praesent vitae risus tincidunt, hendrerit turpis varius, porta augue. Cras vehicula hendrerit molestie.",
        maxPlayers = "4",
        playingTime = "120",
        minAge = "14",
        designers = listOf("Person 1"),
        artists = listOf("Person 1"),
        publishers = listOf("Person 1"),
    ),
    Game(
        image = "https://cf.geekdo-images.com/yetm7Pm1q8pHZHKzTdbRzQ__imagepage/img/8EJjHxMK5fnSdDBB0eZimxeiYBA=/fit-in/900x600/filters:no_upscale():strip_icc()/pic6555174.png",
        name = "Game A with a very large board game name",
        yearPublished = "2022",
        description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultrices sapien pharetra quam rutrum, et aliquam libero facilisis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam risus dui, semper sit amet lectus ac, euismod vestibulum ligula. Nulla quis quam nec lectus condimentum tristique in sit amet nibh. Vestibulum dui orci, facilisis non mauris eu, maximus venenatis quam. Suspendisse ut diam cursus, consequat diam non, pellentesque quam. Praesent vitae risus tincidunt, hendrerit turpis varius, porta augue. Cras vehicula hendrerit molestie.",
        maxPlayers = "4",
        playingTime = "120",
        minAge = "14",
        designers = listOf("Person 1"),
        artists = listOf("Person 1"),
        publishers = listOf("Person 1"),
    )
)
