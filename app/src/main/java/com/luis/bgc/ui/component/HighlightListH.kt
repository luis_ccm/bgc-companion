package com.luis.bgc.ui.component

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.luis.bgc.domain.model.Game
import com.luis.bgc.ui.screens.home.provideGameList
import com.luis.bgc.ui.theme.AppTheme
import com.luis.bgc.R

@Composable
fun HighlightListH(
    gameList: List<Game>,
    modifier: Modifier = Modifier,
    onItemClick: (Game) -> Unit = {}
) {
    LazyRow(
        modifier = modifier,
        horizontalArrangement = Arrangement.spacedBy(16.dp),
        contentPadding = PaddingValues(16.dp),
    ) {
        itemsIndexed(gameList) { index, item ->
            HighlightItemH(item, index, onItemClick)
        }
    }
}

@Composable
fun HighlightItemH(
    game: Game,
    index: Int,
    onItemClick: (Game) -> Unit = {}
) {
    Column(
        modifier = Modifier
            .width(128.dp)
            .clickable { onItemClick(game) },
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Box {
            AsyncImage(
                modifier = Modifier
                    .width(128.dp)
                    .height(160.dp)
                    .clip(RoundedCornerShape(4.dp)),
                model = game.image,
                contentDescription = null,
                contentScale = ContentScale.Crop,
                placeholder = painterResource(R.drawable.placeholder_image),
                error = painterResource(R.drawable.error_image),
            )
            Text(
                modifier = Modifier
                    .width(24.dp)
                    .height(24.dp)
                    .background(
                        color = MaterialTheme.colorScheme.secondary,
                        shape = RoundedCornerShape(0.dp, 0.dp, 8.dp, 0.dp)
                    )
                    .padding(top = 1.dp),
                text = (index + 1).toString(),
                style = MaterialTheme.typography.titleSmall,
                textAlign = TextAlign.Center,
                color = MaterialTheme.colorScheme.onSecondary
            )
        }
        Text(
            modifier = Modifier.padding(top = 4.dp),
            text = game.name ?: "---",
            style = MaterialTheme.typography.titleMedium,
            textAlign = TextAlign.Center,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
        )
    }
}

@Preview(showBackground = true)
@Composable
fun HighlightListHPreview() {
    AppTheme {
        HighlightListH(
            modifier = Modifier.fillMaxWidth(),
            gameList = provideGameList(),
        )
    }
}

@Preview(showBackground = true)
@Composable
fun HighlightItemHPreview() {
    AppTheme {
        HighlightItemH(
            game = provideGameList().first(),
            1
        )
    }
}
