package com.luis.bgc.ui.screens.main

import androidx.annotation.StringRes
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountBox
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.outlined.Home
import androidx.compose.material.icons.outlined.StarRate
import androidx.compose.ui.graphics.vector.ImageVector
import com.luis.bgc.R
import com.luis.bgc.ui.COLLECTION_SCREEN
import com.luis.bgc.ui.HOME_SCREEN
import com.luis.bgc.ui.PROFILE_SCREEN

sealed class MainScreens(val route: String, @StringRes val resourceId: Int, val icon: ImageVector) {
    object Home : MainScreens(HOME_SCREEN, R.string.bottom_nav_home_label, Icons.Outlined.Home)
    object Collection : MainScreens(COLLECTION_SCREEN, R.string.bottom_nav_collection_label, Icons.Outlined.StarRate)
//    object Profile : MainScreens(PROFILE_SCREEN, R.string.bottom_nav_profile_label, Icons.Default.AccountBox)
}
