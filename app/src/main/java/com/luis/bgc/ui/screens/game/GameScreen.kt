package com.luis.bgc.ui.screens.game

import androidx.compose.animation.*
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.BlurredEdgeTreatment
import androidx.compose.ui.draw.blur
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.text.HtmlCompat
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavHostController
import coil.compose.AsyncImage
import com.luis.bgc.R
import com.luis.bgc.domain.model.Game
import com.luis.bgc.ui.component.*
import com.luis.bgc.ui.screens.home.provideGameList
import com.luis.bgc.ui.util.AppThemeWithSurface
import com.luis.bgc.ui.util.ProgressScreen


@Composable
fun GameScreen(
    appNavController: NavHostController,
    gameId: String,
    modifier: Modifier = Modifier,
    viewModel: GameViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiStateFlow.collectAsStateWithLifecycle()
    viewModel.loadGame(gameId)

    BoxWithPattern {
        GameScreen(
            uiState = uiState,
            modifier = modifier,
            onBackPressed = {
                viewModel.onBackPressed(appNavController)
            }
        )
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun GameScreen(
    uiState: GameUiState,
    modifier: Modifier = Modifier,
    onBackPressed: () -> Unit = {}
) {
    AnimatedContent(
        targetState = uiState,
        transitionSpec = { fadeIn() with fadeOut() }
    ) { targetState ->
        when (targetState) {
            GameUiState.Loading -> ProgressScreen()
            is GameUiState.Success -> GameScreenContent(
                game = targetState.game,
                modifier = modifier,
                onBackPressed = onBackPressed
            )
        }
    }
}

@Composable
fun GameScreenContent(
    game: Game,
    modifier: Modifier = Modifier,
    onBackPressed: () -> Unit = {},
) {
    LazyColumn(
        modifier = modifier.fillMaxSize()
    ) {
        item {
            GameHeader(
                gameImageUrl = game.image ?: "",
                gameName = game.name ?: "",
                gameDesigners = game.designers ?: emptyList(),
                gameArtists = game.artists ?: emptyList(),
                gamePublishers = game.publishers ?: emptyList(),
                onBackPressed = onBackPressed
            )
        }
        item {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceEvenly,
            ) {
                HighlightedInfo(stringResource(id = R.string.game_page_play_time_info), "${game.minPlayTime} - ${game.maxPlayTime}")
                HighlightedInfo(stringResource(id = R.string.game_page_players_info), "${game.minPlayers} - ${game.maxPlayers}")
                HighlightedInfo(stringResource(id = R.string.game_page_year_published_info), game.yearPublished.toString())
            }
        }
        item {
            GamePageSectionTitle(text = stringResource(id = R.string.game_page_description))

            var description = game.description.orEmpty()
            val breakLineIndex = description.indexOf("&#10")
            if (breakLineIndex > 0) {
                description = description.substring(0, breakLineIndex)
            }
            Text(
                modifier = Modifier.padding(16.dp, 8.dp, 16.dp, 0.dp),
                text = HtmlCompat.fromHtml(description, HtmlCompat.FROM_HTML_MODE_LEGACY).toString(),
                style = MaterialTheme.typography.labelSmall
            )
        }
        item {
            game.categories.takeIf { it?.isNotEmpty() == true }?.let { categories ->
                GamePageSectionTitle(text = stringResource(id = R.string.game_page_categories))
                FlowAssistChips(itemList = categories)
            }
        }
        item {
            game.expansions.takeIf { it?.isNotEmpty() == true }?.let { expansions ->
                GamePageSectionTitle(text = stringResource(id = R.string.game_page_expansions))
                FlowAssistChips(itemList = expansions)
            }
        }
    }
}

@Composable
fun GameHeader(
    gameImageUrl: String,
    gameName: String,
    gameDesigners: List<String>,
    gameArtists: List<String>,
    gamePublishers: List<String>,
    modifier: Modifier = Modifier,
    onBackPressed: () -> Unit = {}
) {
    Box(modifier = modifier.fillMaxWidth()) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight(),
            contentAlignment = Alignment.TopCenter
        ) {
            AsyncImage(
                model = gameImageUrl,
                contentDescription = null,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(130.dp)
                    .blur(
                        radiusX = 2.dp,
                        radiusY = 2.dp,
                        edgeTreatment = BlurredEdgeTreatment(RectangleShape)
                    ),
                contentScale = ContentScale.Crop,
                placeholder = painterResource(R.drawable.placeholder_image),
                error = painterResource(R.drawable.error_image),
            )
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(220.dp)
                    .padding(top = 40.dp)
                    .background(
                        brush = Brush.verticalGradient(
                            startY = 0f,
                            colors = listOf(Color.Transparent, MaterialTheme.colorScheme.background, Color.Transparent),
                        )
                    ),
            )
        }
        IconButton(onClick = { onBackPressed() }) {
            Icon(
                imageVector = Icons.Filled.ArrowBack,
                contentDescription = null
            )
        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .padding(16.dp, 70.dp, 16.dp, 16.dp),
            verticalAlignment = Alignment.Bottom
        ) {
            AsyncImage(
                model = gameImageUrl,
                contentDescription = null,
                modifier = Modifier
                    .width(140.dp)
                    .height(180.dp)
                    .clip(RoundedCornerShape(4.dp))
                    .shadow(4.dp),
                contentScale = ContentScale.Crop,
                placeholder = painterResource(R.drawable.placeholder_image),
                error = painterResource(R.drawable.error_image),
            )
            Column(
                modifier = Modifier
                    .wrapContentHeight()
                    .padding(start = 16.dp),
                verticalArrangement = Arrangement.Bottom
            ) {
                Text(
                    modifier = Modifier.padding(bottom = 8.dp),
                    text = gameName,
                    style = MaterialTheme.typography.titleLarge,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis
                )
                RowTitleInfo(stringResource(R.string.game_page_designer_info), formatGameInfoList(gameDesigners))
                RowTitleInfo(stringResource(R.string.game_page_artist_info), formatGameInfoList(gameArtists))
                RowTitleInfo(stringResource(R.string.game_page_publisher_info), formatGameInfoList(gamePublishers))
            }
        }
    }
}

fun formatGameInfoList(infoList: List<String>): String =
    if (infoList.count() == 1) {
        infoList.first()
    } else {
        "${infoList.first()} + ${infoList.count() - 1}"
    }

/*@Composable
@Preview
fun GameHeaderPreview() {
    AppThemeWithSurface {
        GameHeader(
            gameImageUrl = "https://cf.geekdo-images.com/yetm7Pm1q8pHZHKzTdbRzQ__imagepage/img/8EJjHxMK5fnSdDBB0eZimxeiYBA=/fit-in/900x600/filters:no_upscale():strip_icc()/pic6555174.png",
            gameName = "Blood Rage With a very big name to have two lines",
            gameDesigner = "P1",
            gameArtist = "P2",
            gamePublisher = "P3",
        )
    }
}*/

@Composable
@Preview(showSystemUi = true)
fun GameScreenPreview() {
    AppThemeWithSurface {
        GameScreen(
            uiState = GameUiState.Success(provideGameList().first())
        )
    }
}
