package com.luis.bgc.ui.component

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.AssistChip
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.google.accompanist.flowlayout.FlowRow

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun FlowAssistChips(
    itemList: List<String>,
    modifier: Modifier = Modifier,
    onClick: (Int) -> Unit = {}
) {
    FlowRow(
        modifier = modifier
            .fillMaxWidth()
            .padding(horizontal = 14.dp)
    ) {
        repeat(itemList.count()) { index ->
            AssistChip(
                modifier = Modifier.padding(horizontal = 2.dp),
                label = { Text(text = itemList[index]) },
                onClick = {}
            )
        }
    }
}
