package com.luis.bgc.ui.component

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.luis.bgc.R
import com.luis.bgc.domain.model.Game
import com.luis.bgc.ui.screens.home.provideGameList
import com.luis.bgc.ui.theme.AppTheme

@Composable
fun HighlightListV(
    modifier: Modifier = Modifier,
    gameList: List<Game>,
) {
    LazyColumn(
        modifier = modifier,
        verticalArrangement = Arrangement.spacedBy(16.dp),
        contentPadding = PaddingValues(all = 16.dp),
    ) {
        items(gameList) {
            HighlightItemV(it)
        }
    }
}

@Composable
fun HighlightItemV(game: Game) {
    Row {
        AsyncImage(
            model = game.image,
            contentDescription = null,
            modifier = Modifier
                .width(48.dp)
                .height(64.dp),
            contentScale = ContentScale.Crop,
            placeholder = painterResource(R.drawable.placeholder_image),
            error = painterResource(R.drawable.error_image),
        )
        Spacer(modifier = Modifier.width(16.dp))
        Column {
            Text(
                text = game.name ?: "---",
                style = MaterialTheme.typography.titleLarge,
            )
            Text(
                text = game.yearPublished ?: "---",
                style = MaterialTheme.typography.bodyLarge,
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun HotItemPreviewV() {
    AppTheme {
        HighlightListV(
            modifier = Modifier.fillMaxWidth(),
            gameList = provideGameList()
        )
    }
}
