package com.luis.bgc.ui.component

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedCard
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.luis.bgc.ui.theme.AppTheme

@Composable
fun HighlightedInfo(
    title: String,
    info: String,
    modifier: Modifier = Modifier
) {
    OutlinedCard(
        modifier = modifier.wrapContentSize(),
        colors = CardDefaults.cardColors(
            containerColor = Color.Transparent
        )
    ) {
        Column(
            modifier = modifier.padding(16.dp, 8.dp),
            verticalArrangement = Arrangement.SpaceAround,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(text = title, style = MaterialTheme.typography.bodySmall)
            Text(text = info, style = MaterialTheme.typography.titleMedium)
        }
    }
}

@Composable
@Preview(showBackground = true)
fun HighlightedInfoPreview() {
    AppTheme {
        HighlightedInfo("Duration", "120 min")
    }
}
