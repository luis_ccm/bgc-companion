package com.luis.bgc.ui.screens.home

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavHostController
import com.luis.bgc.R
import com.luis.bgc.domain.model.Game
import com.luis.bgc.domain.use_case.GetHotBoardGames
import com.luis.bgc.ui.GAME_SCREEN
import com.luis.bgc.ui.util.SnackbarManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getHotBoardGames: GetHotBoardGames,
) : ViewModel() {

    private val _uiStateFlow = MutableStateFlow<HomeUiState>(HomeUiState.Loading)
    val uiStateFlow: StateFlow<HomeUiState>
        get() = _uiStateFlow

    init {
        loadHotItems()
    }

    private fun loadHotItems() {
        viewModelScope.launch {
            getHotBoardGames()
                .catch {
                    Log.d("luis TEST", "FAILURE $it")
                    SnackbarManager.showMessage(R.string.generic_error)
                    _uiStateFlow.value = HomeUiState.Success(emptyList())
                }
                .collect { _uiStateFlow.value = HomeUiState.Success(it) }
        }
    }

    fun onItemClick(appNavController: NavHostController, game: Game) =
        appNavController.navigate("$GAME_SCREEN/${game.bggId}")
}
