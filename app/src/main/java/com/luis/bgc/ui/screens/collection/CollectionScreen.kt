package com.luis.bgc.ui.screens.collection

import androidx.compose.animation.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.*
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavHostController
import com.luis.bgc.R
import com.luis.bgc.domain.model.Game
import com.luis.bgc.ui.component.AlertFilters
import com.luis.bgc.ui.component.AlertWithInput
import com.luis.bgc.ui.component.BoxWithPattern
import com.luis.bgc.ui.component.GamesGrid
import com.luis.bgc.ui.screens.home.provideGameList
import com.luis.bgc.ui.util.AppThemeWithSurface
import com.luis.bgc.ui.util.ProgressScreen

@Composable
fun CollectionScreen(
    appNavController: NavHostController,
    modifier: Modifier = Modifier,
    viewModel: CollectionViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiStateFlow.collectAsStateWithLifecycle()

    BoxWithPattern {
        CollectionScreen(
            uiState = uiState,
            modifier = modifier,
            onItemClick = { viewModel.onItemClick(appNavController, it) },
            onBggUsernameSubmit = { viewModel.onBggUsernameSubmit(it) },
            onFilterCollectionSubmit = { viewModel.onFilterCollectionSubmit(it) }
        )
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun CollectionScreen(
    uiState: CollectionUiState,
    modifier: Modifier = Modifier,
    onItemClick: (Game) -> Unit = {},
    onBggUsernameSubmit: (String) -> Unit = {},
    onFilterCollectionSubmit: (Int) -> Unit = {},
) {
    val showBggUsernameDialog = remember { mutableStateOf(false) }
    val showFilterDialog = remember { mutableStateOf(false) }

    AnimatedContent(
        targetState = uiState,
        transitionSpec = { fadeIn() with fadeOut() }
    ) { targetState ->
        when (targetState) {
            CollectionUiState.Loading -> ProgressScreen()
            is CollectionUiState.Success -> {
                if (showBggUsernameDialog.value) {
                    AlertWithInput(
                        title = stringResource(id = R.string.collection_bgg_alert_title),
                        hint = stringResource(id = R.string.collection_input_bgg_username_hint),
                        initialText = targetState.bggUsername,
                        onSubmit = {
                            showBggUsernameDialog.value = false
                            onBggUsernameSubmit(it)
                        },
                        onDismiss = { showBggUsernameDialog.value = false }
                    )
                }

                /*if (showFilterDialog.value) {
                    AlertFilters(
                        title = stringResource(id = R.string.collection_filter_alert_title),
                        onSubmit = {
                            showFilterDialog.value = false
                            onFilterCollectionSubmit(it)
                        },
                        onDismiss = { showFilterDialog.value = false }
                    )
                }*/

                Column {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween,
                    ) {
                        Text(
                            modifier = Modifier.padding(16.dp),
                            text = if (targetState.bggUsername.isNotEmpty()) {
                                stringResource(R.string.collection_title, targetState.bggUsername)
                            } else {
                                ""
                            },
                            style = MaterialTheme.typography.titleLarge
                        )
                        Row {
                            /*IconButton(
                                modifier = Modifier.padding(top = 8.dp, end = 8.dp, bottom = 8.dp),
                                onClick = { showFilterDialog.value = true },
                            ) {
                                Icon(
                                    imageVector = Icons.Outlined.FilterAlt,
                                    contentDescription = null
                                )
                            }*/
                            IconButton(
                                modifier = Modifier.padding(top = 8.dp, end = 8.dp, bottom = 8.dp),
                                onClick = { showBggUsernameDialog.value = true },
                            ) {
                                Image(
                                    modifier = Modifier.clip(CircleShape),
                                    painter = painterResource(id = R.drawable.bgg_icon),
                                    contentDescription = null,
                                )
                            }
                        }
                    }

                    if (targetState.games.isNotEmpty()) {
                        GamesGrid(
                            modifier = modifier,
                            games = targetState.games,
                            onItemClick = onItemClick,
                            ranked = false
                        )
                    } else {
                        Row(
                            modifier = Modifier
                                .padding(16.dp)
                                .fillMaxSize(),
                            horizontalArrangement = Arrangement.Center,
                            verticalAlignment = CenterVertically
                        ) {
                            Icon(
                                imageVector = if (targetState.bggUsername.isNotEmpty()) {
                                    Icons.Outlined.ThumbUp
                                } else {
                                    Icons.Outlined.Info
                                },
                                contentDescription = null
                            )
                            Text(
                                modifier = Modifier
                                    .padding(start = 16.dp)
                                    .widthIn(max = 200.dp),
                                text = if (targetState.bggUsername.isNotEmpty()) {
                                    stringResource(id = R.string.collection_empty_list)
                                } else {
                                    stringResource(id = R.string.collection_no_bgg_username)
                                },
                                style = MaterialTheme.typography.labelSmall
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
@Preview
fun CollectionScreenPreview() {
    AppThemeWithSurface {
        CollectionScreen(uiState = CollectionUiState.Success(provideGameList(), "luisel"))
    }
}
