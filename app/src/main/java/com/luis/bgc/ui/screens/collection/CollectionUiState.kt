package com.luis.bgc.ui.screens.collection

import com.luis.bgc.domain.model.Game

sealed interface CollectionUiState {
    object Loading : CollectionUiState
    data class Success(val games: List<Game>, val bggUsername: String) : CollectionUiState
}
