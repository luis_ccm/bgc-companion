package com.luis.bgc.ui.screens.main

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.luis.bgc.ui.component.MainNavigationBar
import com.luis.bgc.ui.screens.collection.CollectionScreen
import com.luis.bgc.ui.screens.home.HomeScreen

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainScreen(appNavController: NavHostController) {
    val navController = rememberNavController()
    Scaffold(bottomBar = { MainNavigationBar(navController = navController) }) { bottomNavPadding ->
        NavHost(navController, startDestination = MainScreens.Home.route, modifier = Modifier.padding(bottomNavPadding)) {
            composable(MainScreens.Home.route) { HomeScreen(appNavController = appNavController) }
            composable(MainScreens.Collection.route) { CollectionScreen(appNavController = appNavController) }
//            composable(MainScreens.Profile.route) { }
        }
    }
}
