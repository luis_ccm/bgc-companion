package com.luis.bgc.ui

const val MAIN_SCREEN = "main"
const val HOME_SCREEN = "home"
const val COLLECTION_SCREEN = "collection"
const val PROFILE_SCREEN = "profile"

const val GAME_SCREEN = "game"
const val GAME_ID = "game_id"
