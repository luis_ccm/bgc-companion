package com.luis.bgc.ui.util

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.luis.bgc.ui.theme.AppTheme

@Composable
fun AppThemeWithSurface(
    content: @Composable () -> Unit
) {
    AppTheme {
        Surface(
            color = MaterialTheme.colorScheme.background
        ) { content() }
    }
}

@Composable
fun ProgressScreen() {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator()
    }
}
