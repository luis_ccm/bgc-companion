package com.luis.bgc.ui.component

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.ImageShader
import androidx.compose.ui.graphics.ShaderBrush
import androidx.compose.ui.graphics.TileMode
import androidx.compose.ui.res.imageResource
import com.luis.bgc.R

@Composable
fun BoxWithPattern(
    modifier: Modifier = Modifier,
    content: @Composable () -> Unit,
) {
    val image = ImageBitmap.imageResource(R.drawable.bg_pattern_inverted)
    val brush = remember(image) { ShaderBrush(ImageShader(image, TileMode.Repeated, TileMode.Repeated)) }
    Box(
        modifier
            .fillMaxSize()
            .background(brush = brush, alpha = 0.05f)
    ) { content() }
}
