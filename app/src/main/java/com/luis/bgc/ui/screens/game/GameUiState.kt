package com.luis.bgc.ui.screens.game

import com.luis.bgc.domain.model.Game

sealed interface GameUiState {
    object Loading : GameUiState
    data class Success(val game: Game) : GameUiState
}
