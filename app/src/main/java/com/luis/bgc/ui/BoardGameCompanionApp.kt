package com.luis.bgc.ui

import android.content.res.Resources
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.ReadOnlyComposable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.luis.bgc.ui.screens.game.GameScreen
import com.luis.bgc.ui.screens.main.MainScreen
import com.luis.bgc.ui.theme.AppTheme
import com.luis.bgc.ui.util.SnackbarManager
import kotlinx.coroutines.CoroutineScope

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BoardGameCompanionApp() {
    AppTheme {
        val appState = rememberAppState()

        Scaffold(
            snackbarHost = {
                SnackbarHost(
                    hostState = appState.snackbarHostState,
                    modifier = Modifier.padding(8.dp),
                    snackbar = { snackbarData ->
                        Snackbar(snackbarData, contentColor = MaterialTheme.colorScheme.onPrimary)
                    }
                )
            }
        ) { innerPaddingModifier ->
            NavHost(
                navController = appState.navController,
                startDestination = MAIN_SCREEN,
                modifier = Modifier.padding(innerPaddingModifier)
            ) {
                boardGameCompanionGraph(appState)
            }
        }
    }
}

@Composable
fun rememberAppState(
    navController: NavHostController = rememberNavController(),
    snackbarHostState: SnackbarHostState = remember { SnackbarHostState() },
    snackbarManager: SnackbarManager = SnackbarManager,
    resources: Resources = resources(),
    coroutineScope: CoroutineScope = rememberCoroutineScope()
) = remember(navController, snackbarManager, resources, coroutineScope) {
    BoardGameCompanionAppState(navController, snackbarHostState, snackbarManager, resources, coroutineScope)
}

@Composable
@ReadOnlyComposable
fun resources(): Resources {
    LocalConfiguration.current
    return LocalContext.current.resources
}

fun NavGraphBuilder.boardGameCompanionGraph(appState: BoardGameCompanionAppState) {
    composable(MAIN_SCREEN) { MainScreen(appState.navController) }
    composable("$GAME_SCREEN/{$GAME_ID}") { backStackEntry ->
        backStackEntry.arguments?.getString(GAME_ID)?.let {
            GameScreen(
                appNavController = appState.navController,
                gameId = it
            )
        }
    }
}
