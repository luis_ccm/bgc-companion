package com.luis.bgc.ui.screens.home

import com.luis.bgc.domain.model.Game

sealed interface HomeUiState {
    object Loading : HomeUiState
    data class Success(val games: List<Game>) : HomeUiState
}
