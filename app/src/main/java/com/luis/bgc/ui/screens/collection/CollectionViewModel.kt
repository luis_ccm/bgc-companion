package com.luis.bgc.ui.screens.collection

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavHostController
import com.luis.bgc.domain.model.Game
import com.luis.bgc.domain.use_case.GetBggUsername
import com.luis.bgc.domain.use_case.GetCollectionByUsername
import com.luis.bgc.domain.use_case.SaveBggUsername
import com.luis.bgc.ui.GAME_SCREEN
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CollectionViewModel @Inject constructor(
    private val getCollectionByUsername: GetCollectionByUsername,
    private val getBggUsername: GetBggUsername,
    private val saveBggUsername: SaveBggUsername,
) : ViewModel() {

    private var gamesCollection: List<Game> = emptyList()

    private val _uiState: MutableStateFlow<CollectionUiState> = MutableStateFlow(CollectionUiState.Loading)
    val uiStateFlow: StateFlow<CollectionUiState>
        get() = _uiState

    init {
        loadCollectionItems()
    }

    private fun loadCollectionItems() {
        val bggUsername = getBggUsername()

        if (bggUsername.isNotBlank()) {
            viewModelScope.launch {
                getCollectionByUsername(bggUsername, true)
                    .catch {
                        Log.d("luis TEST", "FAILURE $it")
                        _uiState.emit(CollectionUiState.Success(emptyList(), bggUsername))
                    }
                    .collect {
                        gamesCollection = it
                        _uiState.emit(CollectionUiState.Success(it, bggUsername))
                    }
            }
        } else {
            viewModelScope.launch {
                _uiState.emit(CollectionUiState.Success(emptyList(), ""))
            }
        }
    }

    fun onItemClick(appNavController: NavHostController, game: Game) =
        appNavController.navigate("$GAME_SCREEN/${game.bggId}")

    fun onBggUsernameSubmit(username: String) {
        viewModelScope.launch {
            saveBggUsername(username)
            loadCollectionItems()
        }
    }

    fun onFilterCollectionSubmit(playerCount: Int) {
        viewModelScope.launch {
            val bggUsername = getBggUsername()
            _uiState.emit(
                CollectionUiState.Success(
                    gamesCollection.filter { game ->
                        (game.minPlayers?.toInt() ?: 0) <= playerCount && (game.maxPlayers?.toInt() ?: 99) >= playerCount
                    }, bggUsername
                )
            )
        }
    }
}
