package com.luis.bgc.ui.component

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics

@Composable
fun AlertFilters(
    title: String = "",
    onDismiss: () -> Unit = {},
    onSubmit: (Int) -> Unit = {}
) {
    val sliderPosition = remember { mutableStateOf(0f) }

    AlertDialog(
        title = { if (title.isNotBlank()) Text(text = title) },
        onDismissRequest = { onDismiss() },
        text = {
            Column {
                Slider(
                    modifier = Modifier
                        .fillMaxWidth()
                        .semantics { contentDescription = "Player Number (Max)" },
                    value = sliderPosition.value,
                    onValueChange = { sliderPosition.value = it },
                    valueRange = 0f..12f,
                    steps = 13
                )
                Text(
                    modifier = Modifier.align(CenterHorizontally),
                    text = sliderPosition.value.toInt().toString(),
                    style = MaterialTheme.typography.titleMedium
                )
            }
        },
        confirmButton = {
            TextButton(onClick = { onSubmit(sliderPosition.value.toInt()) }) { Text(text = "Submit") }
        }
    )
}
