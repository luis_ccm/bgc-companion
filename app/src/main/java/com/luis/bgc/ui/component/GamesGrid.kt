package com.luis.bgc.ui.component

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.luis.bgc.R
import com.luis.bgc.domain.model.Game

@Composable
fun GamesGrid(
    games: List<Game>,
    modifier: Modifier = Modifier,
    onItemClick: (Game) -> Unit = {},
    ranked: Boolean = false,
    gridItemMinSize: Dp = 116.dp,
    gridItemHeight: Dp = 160.dp
) {
    LazyVerticalGrid(
        modifier = modifier,
        columns = GridCells.Adaptive(minSize = gridItemMinSize),
        verticalArrangement = Arrangement.spacedBy(8.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        contentPadding = PaddingValues(8.dp)
    ) {
        itemsIndexed(games) { index, game ->
            Box(modifier = Modifier.clickable { onItemClick(game) }) {
                AsyncImage(
                    modifier = Modifier
                        .height(gridItemHeight)
                        .clip(RoundedCornerShape(4.dp)),
                    model = game.image,
                    contentDescription = null,
                    contentScale = ContentScale.Crop,
                    placeholder = painterResource(R.drawable.placeholder_image),
                    error = painterResource(R.drawable.error_image),
                )
                if (ranked) {
                    Text(
                        modifier = Modifier
                            .width(24.dp)
                            .height(24.dp)
                            .background(
                                color = MaterialTheme.colorScheme.secondaryContainer,
                                shape = RoundedCornerShape(4.dp, 0.dp, 4.dp, 0.dp)
                            )
                            .padding(top = 1.dp),
                        text = (index + 1).toString(),
                        style = MaterialTheme.typography.titleSmall,
                        textAlign = TextAlign.Center,
                        color = MaterialTheme.colorScheme.onSecondaryContainer
                    )
                }
            }
        }
    }
}
