package com.luis.bgc.domain.use_case

import android.content.SharedPreferences
import javax.inject.Inject

const val KEY_BGG_USERNAME = "KEY_BGG_USERNAME"

class SaveBggUsername @Inject constructor(
    private val sharedPreferences: SharedPreferences
) {

    operator fun invoke(username: String) {
        with(sharedPreferences.edit()) {
            putString(KEY_BGG_USERNAME, username)
            apply()
        }
    }
}
