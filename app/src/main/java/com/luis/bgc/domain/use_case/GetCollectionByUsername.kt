package com.luis.bgc.domain.use_case

import com.luis.bgc.data.repository.BGGRepository
import com.luis.bgc.di.IoDispatcher
import com.luis.bgc.domain.model.Game
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetCollectionByUsername @Inject constructor(
    private val bggRepository: BGGRepository,
    @IoDispatcher private val defaultDispatcher: CoroutineDispatcher = Dispatchers.Default
) {

    suspend operator fun invoke(username: String, own: Boolean): Flow<List<Game>> =
        withContext(defaultDispatcher) { bggRepository.collectionBoardGamesByUsername(username) }

    private fun mapOwn(own: Boolean): Int = if (own) 1 else 0
}
