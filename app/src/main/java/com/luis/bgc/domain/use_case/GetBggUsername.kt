package com.luis.bgc.domain.use_case

import android.content.SharedPreferences
import javax.inject.Inject

class GetBggUsername @Inject constructor(
    private val sharedPreferences: SharedPreferences
) {

    operator fun invoke(): String = sharedPreferences.getString(KEY_BGG_USERNAME, "") ?: ""

}
