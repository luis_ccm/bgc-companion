package com.luis.bgc.data.room.dao

import androidx.room.*
import com.luis.bgc.data.room.entity.GameEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface GameDao {

    @Query("SELECT * FROM games")
    fun getAll(): Flow<List<GameEntity>>

    @Query("SELECT * FROM games WHERE bggId = :bggId LIMIT 1")
    fun findByBggId(bggId: String): Flow<GameEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg games: GameEntity)

    @Delete
    fun delete(game: GameEntity)
}
