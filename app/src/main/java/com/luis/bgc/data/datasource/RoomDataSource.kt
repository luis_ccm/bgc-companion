package com.luis.bgc.data.datasource

import com.luis.bgc.data.room.dao.GameDao
import com.luis.bgc.data.room.dao.TrendingGamesBGGDao
import com.luis.bgc.data.room.dao.UserCollectionDao
import com.luis.bgc.data.room.entity.GameEntity
import com.luis.bgc.data.room.entity.TrendingGameBGGEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class RoomDataSource @Inject constructor(
    private val gameDao: GameDao,
    private val trendingGamesBGGDao: TrendingGamesBGGDao,
    private val userCollectionDao: UserCollectionDao,
) {

    // Game

    fun getAllGames(): Flow<List<GameEntity>> = gameDao.getAll()

    fun getGameByBggId(bggId: String): Flow<GameEntity> = gameDao.findByBggId(bggId)

    suspend fun insertAllGames(games: List<GameEntity>) = gameDao.insertAll(*games.toTypedArray())

    // Trending

    fun getAllTrendingGames(): Flow<List<TrendingGameBGGEntity>> = trendingGamesBGGDao.getAll()

    suspend fun insertAllTrendingGames(games: List<TrendingGameBGGEntity>) = trendingGamesBGGDao.insertAll(*games.toTypedArray())

    fun deleteAllTrendingGames() = trendingGamesBGGDao.deleteAll()

}
