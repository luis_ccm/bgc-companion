package com.luis.bgc.data.util

suspend fun <T> safeApiCall(apiCall: suspend () -> T): Result<T> {
    return try {
        Result.success(apiCall())
    } catch (throwable: Throwable) {
        Result.failure(throwable)
    }
}
