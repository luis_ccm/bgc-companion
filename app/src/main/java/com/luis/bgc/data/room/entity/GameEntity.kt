package com.luis.bgc.data.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.luis.bgc.domain.model.Game

@Entity(tableName = "games")
data class GameEntity(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val bggId: String? = null,
    val name: String? = null,
    val image: String? = null,
    val description: String? = null,
    val yearPublished: String? = null,
    val minPlayers: String? = null,
    val maxPlayers: String? = null,
    val playingTime: String? = null,
    val minPlayTime: String? = null,
    val maxPlayTime: String? = null,
    val minAge: String? = null,
    val designers: List<String>? = null,
    val artists: List<String>? = null,
    val publishers: List<String>? = null,
    val categories: List<String>? = null,
    val mechanics: List<String>? = null,
    val expansions: List<String>? = null,
) {
    fun toGame() = Game(
        id = this.id,
        bggId = this.bggId,
        name = this.name,
        image = this.image,
        description = this.description,
        yearPublished = this.yearPublished,
        minPlayers = this.minPlayers,
        maxPlayers = this.maxPlayers,
        playingTime = this.playingTime,
        minPlayTime = this.minPlayTime,
        maxPlayTime = this.maxPlayTime,
        minAge = this.minAge,
        designers = this.designers,
        artists = this.artists,
        publishers = this.publishers,
        categories = this.categories,
        mechanics = this.mechanics,
        expansions = this.expansions,
    )
}
