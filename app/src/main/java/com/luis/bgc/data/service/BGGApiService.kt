package com.luis.bgc.data.service

import com.luis.bgc.data.response.CollectionItemsApiResponse
import com.luis.bgc.data.response.HotItemsApiResponse
import com.luis.bgc.data.response.ThingItemsApiResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface BGGApiService {

    @GET("hot")
    suspend fun fetchHotGames(): HotItemsApiResponse

    @GET("collection")
    suspend fun fetchCollectionByUsername(
        @Query("username") username: String,
        @Query("own") own: Int
    ): CollectionItemsApiResponse

    @GET("thing")
    suspend fun fetchGameById(
        @Query("id") id: String
    ): ThingItemsApiResponse

}
