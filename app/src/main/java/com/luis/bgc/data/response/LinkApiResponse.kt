package com.luis.bgc.data.response

import org.simpleframework.xml.Attribute

data class LinkApiResponse @JvmOverloads constructor(
    @field:Attribute(name = "type", required = false)
    @param:Attribute(name = "type", required = false)
    val type: String? = null,
    @field:Attribute(name = "id", required = false)
    @param:Attribute(name = "id", required = false)
    val id: String? = null,
    @field:Attribute(name = "value", required = false)
    @param:Attribute(name = "value", required = false)
    val value: String? = null
)
