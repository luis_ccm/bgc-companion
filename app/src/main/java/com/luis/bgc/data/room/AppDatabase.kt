package com.luis.bgc.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.luis.bgc.data.room.converter.StringListConverter
import com.luis.bgc.data.room.dao.GameDao
import com.luis.bgc.data.room.dao.TrendingGamesBGGDao
import com.luis.bgc.data.room.dao.UserCollectionDao
import com.luis.bgc.data.room.entity.GameEntity
import com.luis.bgc.data.room.entity.TrendingGameBGGEntity
import com.luis.bgc.data.room.entity.UserCollectionEntity

@Database(entities = [GameEntity::class, UserCollectionEntity::class, TrendingGameBGGEntity::class], version = 1)
@TypeConverters(StringListConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun gameDao(): GameDao
    abstract fun userCollectionDao(): UserCollectionDao
    abstract fun trendingGameBGGDao(): TrendingGamesBGGDao
}
