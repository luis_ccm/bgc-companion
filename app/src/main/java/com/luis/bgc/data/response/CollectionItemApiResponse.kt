package com.luis.bgc.data.response

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "item", strict = false)
data class CollectionItemApiResponse @JvmOverloads constructor(
    @field:Attribute(name = "objectid", required = false)
    @param:Attribute(name = "objectid", required = false)
    val objectid: String? = null,
    @field:Element(name = "name", required = false)
    @param:Element(name = "name", required = false)
    val name: String? = null,
    @field:Element(name = "yearpublished", required = false)
    @param:Element(name = "yearpublished", required = false)
    val yearPublished: String? = null,
    @field:Element(name = "image", required = false)
    @param:Element(name = "image", required = false)
    val image: String? = null,
    @field:Element(name = "thumbnail", required = false)
    @param:Element(name = "thumbnail", required = false)
    val thumbnail: String? = null,
//    @field:Element(name = "status", required = false)
//    @param:Element(name = "status", required = false)
//    val status: Value? = null,
    @field:Element(name = "numplays", required = false)
    @param:Element(name = "numplays", required = false)
    val numplays: String? = null,
)
