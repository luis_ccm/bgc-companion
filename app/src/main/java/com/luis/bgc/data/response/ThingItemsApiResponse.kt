package com.luis.bgc.data.response

import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "items", strict = false)
data class ThingItemsApiResponse(
    @field:ElementList(name = "item", inline = true, required = false)
    @param:ElementList(name = "item", inline = true, required = false)
    val itemList: List<ThingItemApiResponse>
)
