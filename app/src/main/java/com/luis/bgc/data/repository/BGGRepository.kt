package com.luis.bgc.data.repository

import android.util.Log
import com.luis.bgc.data.datasource.BGGRemoteDataSource
import com.luis.bgc.data.datasource.RoomDataSource
import com.luis.bgc.data.response.CollectionItemApiResponse
import com.luis.bgc.data.response.HotItemApiResponse
import com.luis.bgc.data.response.ThingItemApiResponse
import com.luis.bgc.domain.model.Game
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

class BGGRepository @Inject constructor(
    private val bggRemoteDataSource: BGGRemoteDataSource,
    private val roomDataSource: RoomDataSource,
) {

    val hotBoardGamesStream: Flow<List<Game>> =
        bggRemoteDataSource
            .hotGamesStream
            .map { apiResponse -> apiResponse.map { item -> item.toGame() } }

    suspend fun getHotBoardGamesStream22(): Flow<List<Game>> =
        roomDataSource
            .getAllTrendingGames()
            .map { apiResponse -> apiResponse.map { item -> item.toGame() } }
            .onEach {
                val trendingGames = bggRemoteDataSource
                    .hotGamesStream()
                    .map { item -> item.toGame() }

                saveTrendingGamesInCache(trendingGames)
            }

    fun collectionBoardGamesByUsernameLocal(username: String): Flow<List<Game>> =
        roomDataSource
            .getAllGames()
            .map { list -> list.map { game -> game.toGame() } }

    fun collectionBoardGamesByUsername(username: String): Flow<List<Game>> =
        bggRemoteDataSource
            .collectionByUsername(username, true)
            .map { apiResponse -> apiResponse.map { item -> item.toGame() } }
//            .onEach { saveTrendingGamesInCache(it) }
            .catch { emit(lastCachedNews()) }

    fun gameById(bggId: String): Flow<Game> =
        bggRemoteDataSource
            .gameById(bggId)
            .map { apiResponse -> apiResponse.toGame() }
            .onEach { saveTrendingGamesInCache(it) }

    private suspend fun saveTrendingGamesInCache(gameList: List<Game>) {
        roomDataSource.deleteAllTrendingGames()
        roomDataSource.insertAllTrendingGames(gameList.map { it.toTrendingGameBGGEntity() })
    }

    private suspend fun saveTrendingGamesInCache(game: Game) {
        roomDataSource.insertAllGames(listOf(game.toGameEntity()))
    }

    private fun lastCachedNews(): List<Game> {
        Log.d("luis TEST", "lastCachedNews")
        return emptyList()
    }

    private fun HotItemApiResponse.toGame() = Game(
        bggId = this.id,
        image = this.thumbnail?.value,
        name = this.name?.value,
        yearPublished = this.yearPublished?.value,
    )

    private fun CollectionItemApiResponse.toGame() = Game(
        bggId = this.objectid,
        image = this.image,
        name = this.name,
        yearPublished = this.yearPublished,
    )

    private fun ThingItemApiResponse.toGame() = Game(
        bggId = this.id,
        image = this.image,
        name = this.name?.first()?.value,
        description = this.description,
        yearPublished = this.yearPublished?.value,
        minPlayers = this.minPlayers?.value,
        maxPlayers = this.maxPlayers?.value,
        playingTime = this.playingTime?.value,
        minPlayTime = this.minPlayTime?.value,
        maxPlayTime = this.maxPlayTime?.value,
        minAge = this.minAge?.value,
        designers = this.link?.filter { it.type == "boardgamedesigner" }?.map { it.value ?: "" },
        artists = this.link?.filter { it.type == "boardgameartist" }?.map { it.value ?: "" },
        publishers = this.link?.filter { it.type == "boardgamepublisher" }?.map { it.value ?: "" },
        categories = this.link?.filter { it.type == "boardgamecategory" }?.map { it.value ?: "" },
        mechanics = this.link?.filter { it.type == "boardgamemechanic" }?.map { it.value ?: "" },
        expansions = this.link?.filter { it.type == "boardgameexpansion" }?.map { it.value ?: "" },
    )
}
