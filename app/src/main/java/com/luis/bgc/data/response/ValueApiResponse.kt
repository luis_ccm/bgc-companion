package com.luis.bgc.data.response

import org.simpleframework.xml.Attribute

data class ValueApiResponse @JvmOverloads constructor(
    @field:Attribute(name = "value", required = false)
    @param:Attribute(name = "value", required = false)
    val value: String? = null
)
