package com.luis.bgc.data.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.luis.bgc.domain.model.Game

@Entity(tableName = "trending_games_bgg")
data class TrendingGameBGGEntity(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val gameBggId: String? = null,
    val gameName: String? = null,
    val gameImage: String? = null,
) {
    fun toGame() = Game(
        id = id,
        bggId = gameBggId,
        name = gameName,
        image = gameImage,
    )
}
