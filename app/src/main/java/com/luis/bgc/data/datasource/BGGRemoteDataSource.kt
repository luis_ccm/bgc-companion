package com.luis.bgc.data.datasource

import com.luis.bgc.data.response.CollectionItemApiResponse
import com.luis.bgc.data.response.HotItemApiResponse
import com.luis.bgc.data.response.ThingItemApiResponse
import com.luis.bgc.data.service.BGGApiService
import com.luis.bgc.di.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Retrofit
import javax.inject.Inject

class BGGRemoteDataSource @Inject constructor(
    private val retrofit: Retrofit,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) {

    suspend fun hotGamesStream(): List<HotItemApiResponse> {
        val games = retrofit
            .create(BGGApiService::class.java)
            .fetchHotGames()
        return games.itemList
    }

    val hotGamesStream: Flow<List<HotItemApiResponse>> = flow {
        val games = retrofit
            .create(BGGApiService::class.java)
            .fetchHotGames()
        emit(games.itemList)
    }.flowOn(ioDispatcher)

    fun collectionByUsername(username: String, own: Boolean): Flow<List<CollectionItemApiResponse>> = flow {
        val games = retrofit
            .create(BGGApiService::class.java)
            .fetchCollectionByUsername(username, mapOwn(own))
        emit(games.itemList)
    }.flowOn(ioDispatcher)

    private fun mapOwn(own: Boolean): Int = if (own) 1 else 0

    fun gameById(bggId: String): Flow<ThingItemApiResponse> = flow {
        val games = retrofit
            .create(BGGApiService::class.java)
            .fetchGameById(bggId)
        emit(games.itemList.first())
    }.flowOn(ioDispatcher)

}
