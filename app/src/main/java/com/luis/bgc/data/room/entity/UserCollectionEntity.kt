package com.luis.bgc.data.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.luis.bgc.domain.model.Game

@Entity(tableName = "user_games")
data class UserCollectionEntity(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val gameBggId: String? = null,
    val gameName: String? = null,
    val gameImage: String? = null,
    val favored: Boolean = false,
) {
    fun toGame() = Game(
        id = id,
        bggId = gameBggId,
        name = gameName,
        image = gameImage,
    )
}
