package com.luis.bgc.data.room.dao

import androidx.room.*
import com.luis.bgc.data.room.entity.UserCollectionEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface UserCollectionDao {

    @Query("SELECT * FROM user_games")
    fun getAll(): Flow<List<UserCollectionEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg games: UserCollectionEntity)

    @Delete
    fun delete(game: UserCollectionEntity)
}
