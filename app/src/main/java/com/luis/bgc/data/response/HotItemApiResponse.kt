package com.luis.bgc.data.response

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "item", strict = false)
data class HotItemApiResponse @JvmOverloads constructor(
    @field:Attribute(name = "id", required = false)
    @param:Attribute(name = "id", required = false)
    val id: String? = null,
    @field:Attribute(name = "rank", required = false)
    @param:Attribute(name = "rank", required = false)
    val rank: String? = null,
    @field:Element(name = "thumbnail", required = false)
    @param:Element(name = "thumbnail", required = false)
    val thumbnail: ValueApiResponse? = null,
    @field:Element(name = "name", required = false)
    @param:Element(name = "name", required = false)
    val name: ValueApiResponse? = null,
    @field:Element(name = "yearpublished", required = false)
    @param:Element(name = "yearpublished", required = false)
    val yearPublished: ValueApiResponse? = null,
)
