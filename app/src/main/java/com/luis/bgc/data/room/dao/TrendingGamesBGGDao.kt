package com.luis.bgc.data.room.dao

import androidx.room.*
import com.luis.bgc.data.room.entity.TrendingGameBGGEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface TrendingGamesBGGDao {

    @Query("SELECT * FROM trending_games_bgg")
    fun getAll(): Flow<List<TrendingGameBGGEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg games: TrendingGameBGGEntity)

    @Delete
    fun delete(game: TrendingGameBGGEntity)

    @Query("DELETE FROM trending_games_bgg")
    fun deleteAll()
}
