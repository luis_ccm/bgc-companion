package com.luis.bgc.data.response

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "item", strict = false)
data class ThingItemApiResponse @JvmOverloads constructor(
    @field:Attribute(name = "id", required = false)
    @param:Attribute(name = "id", required = false)
    val id: String? = null,
    @field:Attribute(name = "type", required = false)
    @param:Attribute(name = "type", required = false)
    val type: String? = null,
    @field:Element(name = "thumbnail", required = false)
    @param:Element(name = "thumbnail", required = false)
    val thumbnail: String? = null,
    @field:Element(name = "image", required = false)
    @param:Element(name = "image", required = false)
    val image: String? = null,
    @field:ElementList(inline = true, entry = "name", required = false)
    @param:ElementList(inline = true, entry = "name", required = false)
    val name: List<ValueApiResponse>? = null,
    @field:Element(name = "description", required = false)
    @param:Element(name = "description", required = false)
    val description: String? = null,
    @field:Element(name = "yearpublished", required = false)
    @param:Element(name = "yearpublished", required = false)
    val yearPublished: ValueApiResponse? = null,
    @field:Element(name = "minplayers", required = false)
    @param:Element(name = "minplayers", required = false)
    val minPlayers: ValueApiResponse? = null,
    @field:Element(name = "maxplayers", required = false)
    @param:Element(name = "maxplayers", required = false)
    val maxPlayers: ValueApiResponse? = null,
    @field:Element(name = "playingtime", required = false)
    @param:Element(name = "playingtime", required = false)
    val playingTime: ValueApiResponse? = null,
    @field:Element(name = "minplaytime", required = false)
    @param:Element(name = "minplaytime", required = false)
    val minPlayTime: ValueApiResponse? = null,
    @field:Element(name = "maxplaytime", required = false)
    @param:Element(name = "maxplaytime", required = false)
    val maxPlayTime: ValueApiResponse? = null,
    @field:Element(name = "minage", required = false)
    @param:Element(name = "minage", required = false)
    val minAge: ValueApiResponse? = null,
    @field:ElementList(inline = true, entry = "link", required = false)
    @param:ElementList(inline = true, entry = "link", required = false)
    val link: List<LinkApiResponse>? = null,
)
